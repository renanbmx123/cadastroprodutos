package br.cadastroprodutos.com.cadastroprodutos.controlador.form;

import br.cadastroprodutos.com.cadastroprodutos.modelo.Produto;
import br.cadastroprodutos.com.cadastroprodutos.repositorio.ProdutoRepositorio;

public class ProdutoForm {
	
	private String nome;
	private String descricao;
	private String categoria;
	private float valor;
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public float getValor() {
		return valor;
	}
	public void setValor(float valor) {
		this.valor = valor;
	}
	public Produto converter() {
		return new Produto(nome,categoria,descricao,valor);
	}
	public Produto atualizar(Long id, ProdutoRepositorio produtoRepositorio) {
		
		Produto produto = produtoRepositorio.getOne(id);
		produto.setNome(this.nome);
		produto.setCategoria(this.categoria);
		produto.setDescricao(this.descricao);
		produto.setValor(this.valor);
		
		return produto;
	}
	
	
}
