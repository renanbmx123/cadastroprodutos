package br.cadastroprodutos.com.cadastroprodutos.controlador.dto;

import java.util.List;
import java.util.stream.Collectors;

import br.cadastroprodutos.com.cadastroprodutos.modelo.Produto;

public class ProdutoDto {
	
	private Long id;
	private String nome;
	private String descricao;
	private String categoria;
	private double valor;
	
	public ProdutoDto(Produto produto) {
		this.id = produto.getId();
		this.nome = produto.getNome();
		this.categoria = produto.getCategoria();
		this.descricao = produto.getDescricao();
		this.valor = produto.getValor();
	}

	public Long getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public String getCategoria() {
		return categoria;
	}

	public double getValor() {
		return valor;
	}
	
	public static List<ProdutoDto> converter(List<Produto> produtos) {		
		return produtos.stream().map(ProdutoDto::new).collect(Collectors.toList());
	}
	
	
}
