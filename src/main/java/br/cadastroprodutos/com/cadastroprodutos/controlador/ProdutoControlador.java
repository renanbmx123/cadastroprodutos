package br.cadastroprodutos.com.cadastroprodutos.controlador;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.cadastroprodutos.com.cadastroprodutos.controlador.dto.ProdutoDto;
import br.cadastroprodutos.com.cadastroprodutos.controlador.form.ProdutoForm;
import br.cadastroprodutos.com.cadastroprodutos.modelo.Produto;
import br.cadastroprodutos.com.cadastroprodutos.repositorio.ProdutoRepositorio;

@RequestMapping("/produtos")
@RestController
public class ProdutoControlador {
	
	@Autowired
	private ProdutoRepositorio produtoRepositorio;
	
	
	@GetMapping
	public List<ProdutoDto> listaProduto() {
		List<Produto> produtos = produtoRepositorio.findAll();
		return ProdutoDto.converter(produtos);
	}
	
	@PutMapping("/{id}")
	@Transactional
	public ResponseEntity<ProdutoDto> atualizar(@PathVariable Long id, ProdutoForm produtoForm ) {
		Optional<Produto> buscaProduto = produtoRepositorio.findById(id);
		if(buscaProduto.isPresent()) {
			Produto produto = produtoForm.atualizar(id, produtoRepositorio);
			return ResponseEntity.ok(new ProdutoDto(produto));
		}
		return ResponseEntity.badRequest().build();
	}
	
	@DeleteMapping("/{id}")
	@Transactional
	public ResponseEntity<?> deletar(@PathVariable Long id){
		Optional<Produto> produto = produtoRepositorio.findById(id);
		if (produto.isPresent()) {
			produtoRepositorio.deleteById(id);
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.badRequest().build();

		
	}
	
	@PostMapping
	@Transactional
	public ResponseEntity<ProdutoDto> cadastrar(@RequestBody ProdutoForm produtoForm, UriComponentsBuilder uriBuilder) {
		
		Produto produto = produtoForm.converter();
		produtoRepositorio.save(produto);
		URI uri = uriBuilder.path("/topicos/{id}").buildAndExpand(produto.getId()).toUri();
		return ResponseEntity.created(uri).body(new ProdutoDto(produto));
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<ProdutoDto> buscaProduto(@PathVariable Long id){
		Optional<Produto> produto = produtoRepositorio.findById(id);
		if (produto.isPresent()) {
			return ResponseEntity.ok(new ProdutoDto(produto.get()));
		}
		return ResponseEntity.notFound().build();
	}
	
	
	
}
