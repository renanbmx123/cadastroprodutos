package br.cadastroprodutos.com.cadastroprodutos.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;

import br.cadastroprodutos.com.cadastroprodutos.modelo.Produto;

public interface ProdutoRepositorio extends JpaRepository<Produto, Long> {

}
