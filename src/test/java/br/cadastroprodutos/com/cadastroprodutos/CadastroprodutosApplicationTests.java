package br.cadastroprodutos.com.cadastroprodutos;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import br.cadastroprodutos.com.cadastroprodutos.controlador.dto.ProdutoDto;
import br.cadastroprodutos.com.cadastroprodutos.modelo.Produto;

@SpringBootTest
class CadastroprodutosApplicationTests {

	@Test
	void contextLoads() {
		
		Produto produto1 = new Produto();
		String nome = "Xicara";
		String categoria = "Outros";
		String descricao = "Xicara de porcelana";
		double valor = 5.0;
				
		produto1.setCategoria(categoria);;
		produto1.setDescricao(descricao);
		produto1.setNome(nome);
		produto1.setValor(valor);
		
		assertEquals(produto1.getNome(), nome);
		assertEquals(produto1.getValor(), valor);
		assertEquals(produto1.getDescricao(), descricao);
		assertEquals(produto1.getCategoria(), categoria);		
	}
	@Test
	void TesteDto() {
		Produto produto1 = new Produto();
		String nome = "Chinelo";
		String categoria = "Calçados";
		String descricao = "Chinelos havainas tamanho 40";
		double valor = 25.0;
		
		produto1.setCategoria(categoria);;
		produto1.setDescricao(descricao);
		produto1.setNome(nome);
		produto1.setValor(valor);
		ProdutoDto prodDto = new ProdutoDto(produto1);
		
		assertEquals(prodDto.getNome(), nome);
		assertEquals(prodDto.getValor(), valor);
		assertEquals(prodDto.getDescricao(), descricao);
		assertEquals(prodDto.getCategoria(), categoria);	
		
	}

}
